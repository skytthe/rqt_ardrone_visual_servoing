#!/usr/bin/env python

import os
import sys
import copy
from threading import Lock

# import QT
from PyQt4 import QtCore

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtGui import QLabel, QTreeWidget, QTreeWidgetItem, QVBoxLayout, QCheckBox, QWidget, QToolBar, QLineEdit, QPushButton
from python_qt_binding.QtCore import Qt, QTimer

# import libs
import math
import PyKDL

# import ROS
import rospy
import roslib

# import ROS msgs
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3
from visualization_msgs.msg import Marker
from ar_track_alvar.msg import AlvarMarker
from ar_track_alvar.msg import AlvarMarkers
from ardrone_visual_servoing.msg import CtrlState
from ardrone_visual_servoing.msg import GoalMarker
from ardrone_visual_servoing.msg import PoseEstimator

# import TF
import tf
import tf.transformations as tfMath
from tf_conversions import posemath

# import
import markers as markers

# constants
VIS_PUB_FREQ =  2.0 #[hz]
REDRAW_FREQ  = 30.0 #[hz]


class ARDroneVisualServoing(Plugin):
   def __init__(self,context):
      # init RQT gui 
      super(ARDroneVisualServoing, self).__init__(context)
      # Give QObjects reasonable names
      self.setObjectName('ARDroneVisualServoing')    

      # Process standalone plugin command-line arguments
      from argparse import ArgumentParser
      parser = ArgumentParser()
      # Add argument(s) to the parser.
      parser.add_argument("-q", "--quiet", action="store_true",
                           dest="quiet",
                           help="Put plugin in silent mode")
      args, unknowns = parser.parse_known_args(context.argv())
#        if not args.quiet:
#            print 'arguments: ', args
#            print 'unknowns: ', unknowns

      # Create QWidget
      self._widget = QWidget()
      # Get path to UI file which is a sibling of this file
      # in this example the .ui and .py file are in the same folder
      ui_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'g.ui')
      # Extend the widget with all attributes and children from UI file
      loadUi(ui_file, self._widget)
      # Give QObjects reasonable names
      self._widget.setObjectName('ARdroneUi')
      # Show _widget.windowTitle on left-top of each plugin (when 
      # it's set in _widget). This is useful when you open multiple
      # plugins at once. Also if you open multiple instances of your 
      # plugin at once, these lines add number to make it easy to 
      # tell from pane to pane.
      if context.serial_number() > 1:
         self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
      # Add widget to the user interface
      context.add_widget(self._widget)

      '''
      init GUI
      '''
      # init pose estimation list
      pose_estimators = ['ARdrone_est','ARdrone_est_lp']
      for s in pose_estimators:
         self._widget.pe_list.addItem(s)
      # connect callbacks
      self._widget.pe_list.currentIndexChanged.connect(self.poseEstCallback)
      self._widget.pe_spinbox_lp.valueChanged.connect(self.poseEstCallback)

      # init goal marker list
      self._widget.gm_list.addItem("ar_marker")
      # connect callbacks
      self._widget.gm_list.currentIndexChanged.connect(self.goalMarkerCallback)
      self._widget.gm_spinbox_id.valueChanged.connect(self.goalMarkerCallback)

      # init event callback for pid section
      self._widget.pid_spinbox_x.valueChanged.connect(self.KpCallback)      
      self._widget.pid_spinbox_y.valueChanged.connect(self.KiCallback)
      self._widget.pid_spinbox_z.valueChanged.connect(self.KdCallback)

      # init event callback for goal pose section
      self._widget.gp_spinbox_x.valueChanged.connect(self.GoalPxCallback)
      self._widget.gp_spinbox_y.valueChanged.connect(self.GoalPyCallback)
      self._widget.gp_spinbox_z.valueChanged.connect(self.GoalPzCallback)


      '''
      init subscribers
      '''
      self.subCtrlState = rospy.Subscriber('/ardrone_visual_servoing/controller_state', CtrlState, self.receiveCtrlState)

      '''
      init publishers
      '''
      # PID gains
      self.pubPid = rospy.Publisher('/ardrone_visual_servoing/pid',Vector3)
      # Goal Pose
      self.pubGoalPose = rospy.Publisher('/ardrone_visual_servoing/goal_pose', Pose)
      # Goal Marker
      self.pubGoalMarker = rospy.Publisher('/ardrone_visual_servoing/goal_marker_frame',GoalMarker)
      # Pose estimation
      self.pubPoseEst = rospy.Publisher('/ardrone_visual_servoing/pose_est',PoseEstimator)

      # ARdrone marker for visualization
      self.pubDroneMarker = rospy.Publisher('/ardrone_dashboard/ARdroneModel',Marker)
      # position error marker for visualization
      self.pubPosErrorMarker = rospy.Publisher('/ardrone_dashboard/PositionError',Marker)
      # manual command marker for visualization
      self.pubPosManualCmdMarker = rospy.Publisher('/ardrone_dashboard/ManualCmd',Marker)
      # auto pilot command marker for visualization
      self.pubPosAutopilotCmdMarker = rospy.Publisher('/ardrone_dashboard/AutopilotCmd',Marker)

      '''
      init variables
      '''
      self.activePoseEstimator = str(self._widget.pe_list.currentText()) 

      # msg
      self.ctrlState = None
      # locks
      self.ctrlStateLock = Lock()
      # Point for visualization markers
      self.errorPoint = None
      self.cmdManualPoint    = Point(0.,0.,0.)
      self.cmdAutoPilotPoint = Point(0.,0.,0.)
 
      '''
      init timers
      '''
      # gui redraw
      self.redrawTimer = QtCore.QTimer(self)
      self.redrawTimer.timeout.connect(self.redrawGuiCallback)
      self.redrawTimer.start(1000.0 * 1.0/REDRAW_FREQ)
      # visualization
      self.visualizationTimer = QtCore.QTimer(self)
      self.visualizationTimer.timeout.connect(self.visualizationCallback)
      self.visualizationTimer.start(1000.0 * 1.0/VIS_PUB_FREQ)

   def receiveCtrlState(self,msg):
      self.ctrlStateLock.acquire()
      try:
         self.ctrlState = msg
      finally:
         self.ctrlStateLock.release()

   def poseEstCallback(self,event):
      self.activePoseEstimator = str(self._widget.pe_list.currentText())      
      msg = PoseEstimator()
      msg.pose_est_frame = self.activePoseEstimator
      msg.lowpass_length = int(self._widget.pe_spinbox_lp.value())
      self.pubPoseEst.publish(msg)

   def goalMarkerCallback(self,event):
      marker_id = self._widget.gm_spinbox_id.value()
      marker_frame = str(self._widget.gm_list.currentText())+"_"+str(marker_id)
      msg = GoalMarker()
      msg.goal_marker_id    = int(marker_id)
      msg.goal_marker_frame = marker_frame
      self.pubGoalMarker.publish(msg)
      print msg

   def KpCallback(self,event): 
      self.pidCallback(event,0)
   def KiCallback(self,event):
      self.pidCallback(event,1)
   def KdCallback(self,event):
      self.pidCallback(event,2)
   def pidCallback(self,event,index):
      self.ctrlStateLock.acquire()
      try:
         pidMsg = copy.deepcopy(self.ctrlState.pid)
      finally:
         self.ctrlStateLock.release()
      gains = [pidMsg.x,pidMsg.y,pidMsg.z]
      if event != gains[index]:
         gains[index] = event
         self.pubPid.publish(Vector3(*gains))

   def GoalPxCallback(self,event):
      self.GoalPoseCallback(event,0)
   def GoalPyCallback(self,event):
      self.GoalPoseCallback(event,1)
   def GoalPzCallback(self,event):
      self.GoalPoseCallback(event,2)
   def GoalPoseCallback(self,event,index):
      self.ctrlStateLock.acquire()
      try:
         posMsg = copy.deepcopy(self.ctrlState.goal_pos.position)
      finally:
         self.ctrlStateLock.release()
      pos = [posMsg.x, posMsg.y, posMsg.z]
      if event != pos[index]:
         pos[index] = event
         newPose = Pose(Point(*pos),Quaternion(0.,0.,0.,1.))
         self.pubGoalPose.publish(newPose)

   def redrawGuiCallback(self):
      if self.ctrlState != None:
         self.ctrlStateLock.acquire()
         try:
            state = copy.deepcopy(self.ctrlState)
         finally:
            self.ctrlStateLock.release()
         
         # redraw controller state section
         self._widget.ctrl_mode.setText(state.controller_state)
         self._widget.ctrl_autopilot.setText(state.autopilot_state)
         if state.calc_error:
            self._widget.ctrl_error_status.setText("")
            self._widget.ctrl_error_x.setText(str(state.pos_error.position.x))
            self._widget.ctrl_error_y.setText(str(state.pos_error.position.y))
            self._widget.ctrl_error_z.setText(str(state.pos_error.position.z))
         else:
            self._widget.ctrl_error_status.setText("Not found")
            self._widget.ctrl_error_x.setText("")
            self._widget.ctrl_error_y.setText("")
            self._widget.ctrl_error_z.setText("")

         # redraw pose estimation section
         ### Nothing to redraw

         # redraw PID section
         if (self._widget.pid_spinbox_x.value != state.pid.x):
            self._widget.pid_spinbox_x.setValue(state.pid.x)
         if (self._widget.pid_spinbox_y.value != state.pid.y):
            self._widget.pid_spinbox_y.setValue(state.pid.y)          
         if (self._widget.pid_spinbox_z.value != state.pid.z):
            self._widget.pid_spinbox_z.setValue(state.pid.z)

         # redraw goal pose section
         gPos = state.goal_pos.position
         if (self._widget.gp_spinbox_x.value != gPos.x):
            self._widget.gp_spinbox_x.setValue(gPos.x)
         if (self._widget.gp_spinbox_y.value != gPos.y):
            self._widget.gp_spinbox_y.setValue(gPos.y)
         if (self._widget.gp_spinbox_z.value != gPos.z):
            self._widget.gp_spinbox_z.setValue(gPos.z)

         # redraw goal marker section
         ### Nothing to redraw

         # update data for publishers in visualization section
         if state.calc_error:
            self.errorPoint = state.pos_error.position
         else:
            self.errorPoint = None
         
         cmdM = state.cmd_manual.linear
         self.cmdManualPoint    = Point(cmdM.x,cmdM.y,cmdM.z)
         
         cmdA = state.cmd_autopilot.linear
         self.cmdAutoPilotPoint = Point(cmdA.x,cmdA.y,cmdA.z)

 
         # publish markers with updated data
         self.visualizationCallback()


   def visualizationCallback(self):
      drone_frame = self.activePoseEstimator
      drone_planar_frame = self.activePoseEstimator+"_planar"
      p_zero = Point(0.,0.,0.)
      # AR.Drone model
      if self._widget.vis_cb_ardrone_model.isChecked():
         self.pubDroneMarker.publish(markers.getARdroneMarker(drone_frame))

      # position error
      if self._widget.vis_cb_pos_error.isChecked() and self.errorPoint != None:
         self.pubPosErrorMarker.publish(markers.getArrow(drone_planar_frame,0.0,p_zero,self.errorPoint,False))

      # manual control command
      if self._widget.vis_cb_manual.isChecked():
         marker = None
         if self._widget.vis_rb_man_arrow.isChecked():
            marker = markers.getArrow(drone_planar_frame,0.33,p_zero,self.cmdManualPoint,True)
         elif self._widget.vis_rb_man_sphere.isChecked():
            marker = markers.getSphere(drone_planar_frame,0.33,self.cmdManualPoint)
         if marker != None:
            self.pubPosManualCmdMarker.publish(marker)
       
      # auto pilot control command
      if self._widget.vis_cb_autopilot.isChecked():
         marker = None
         if self._widget.vis_rb_auto_arrow.isChecked():
            marker = markers.getArrow(drone_planar_frame,0.66,p_zero,self.cmdAutoPilotPoint,True)
         elif self._widget.vis_rb_auto_sphere.isChecked():
            marker = markers.getSphere(drone_planar_frame,0.66,self.cmdAutoPilotPoint)
         if marker != None:
            self.pubPosAutopilotCmdMarker.publish(marker)

   def shutdown_plugin(self):
      pass

   def save_settings(self, plugin_settings, instance_settings):
      pass

   def restore_settings(self, plugin_settings, instance_settings):
      pass

   #def trigger_configuration(self):
      # Comment in to signal that the plugin has a way to configure it
      # Usually used to open a configuration dialog
                                               
